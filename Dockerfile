# nvidia/cuda:11.1. Specify hash:
FROM nvcr.io/nvidia/k8s/cuda-sample:nbody-cuda11.1

LABEL maintainer="support@flywheel.io"

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}
ENTRYPOINT ["/tmp/sample", "-gpu", "-benchmark"]